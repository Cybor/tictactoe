package com.anndefined.cybor.tictacline.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.anndefined.cybor.tictacline.R;
import com.anndefined.cybor.tictacline.data.Settings;

import java.util.concurrent.Executors;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class PreloaderActivity extends Activity implements View.OnClickListener {
    private Intent gameMenuIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preloader_layout);

        TextView appNameTV = findViewById(R.id.app_name_tv);
        appNameTV.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/edo.ttf"));

        appNameTV.animate().alpha(1)
                .withEndAction(() -> appNameTV.setOnClickListener(this))
                .start();


        gameMenuIntent = new Intent(this, GameMenuActivity.class)
                .addFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);

        Executors.newSingleThreadExecutor()
                .execute(() -> {
                    try {
                        Settings.getInstance(this);
                        Thread.sleep(2000);
                        startActivity(gameMenuIntent);
                        finish();
                    } catch (InterruptedException e) {
                    }
                });
    }

    @Override
    public void onClick(View view) {
        startActivity(gameMenuIntent);
        finish();
    }
}
