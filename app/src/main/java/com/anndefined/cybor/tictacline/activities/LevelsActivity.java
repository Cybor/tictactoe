package com.anndefined.cybor.tictacline.activities;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.anndefined.cybor.tictacline.R;
import com.anndefined.cybor.tictacline.data.Settings;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class LevelsActivity extends Activity implements View.OnClickListener {
    private View levelsPage1, levelsPage2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.levels_layout);


        ((TextView) findViewById(R.id.app_name_tv))
                .setTypeface(Typeface.createFromAsset(getAssets(), "fonts/edo.ttf"));
        ((TextView) findViewById(R.id.screen_header_tv))
                .setTypeface(Typeface.createFromAsset(getAssets(), "fonts/schoolbell.ttf"));

        levelsPage1 = findViewById(R.id.levels_page1);
        levelsPage2 = findViewById(R.id.levels_page2);

        TextView closeButton = findViewById(R.id.close_button);
        closeButton.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/edo.ttf"));
        closeButton.setOnClickListener(this);

        setButtons(R.id.back_button,
                R.id.next_button,

                R.id.beginner_button,
                R.id.novice_button,
                R.id.intermediate_button,
                R.id.advanced_button,
                R.id.expert_button);
    }

    private void setButtons(@IdRes int... ids) {
        for (@IdRes int id : ids)
            findViewById(id).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.next_button:
                if (levelsPage2.getVisibility() == GONE) {
                    levelsPage1.setVisibility(GONE);
                    levelsPage2.setVisibility(VISIBLE);
                }
                break;
            case R.id.close_button:
                finish();
                break;
            default:
                Settings.getInstance(this)
                        .setFieldSize(Byte.parseByte(view.getTag().toString()))
                        .save();
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (levelsPage1.getVisibility() == GONE) {
            levelsPage2.setVisibility(GONE);
            levelsPage1.setVisibility(VISIBLE);
        } else super.onBackPressed();
    }
}
