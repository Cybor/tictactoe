package com.anndefined.cybor.tictacline.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anndefined.cybor.tictacline.R;

import java.util.HashMap;
import java.util.concurrent.Executors;

import static android.view.View.VISIBLE;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.anndefined.cybor.tictacline.activities.GameActivity.TAG_DRAW;
import static com.anndefined.cybor.tictacline.activities.GameActivity.TAG_EMPTY;

public class OverviewActivity extends Activity implements View.OnClickListener {
    private final int MAX_ROUNDS = 5;
    private LinearLayout roundsContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview_layout);

        AssetManager assets = getAssets();
        Typeface schoolbell = Typeface.createFromAsset(assets, "fonts/schoolbell.ttf");
        Typeface glegoo = Typeface.createFromAsset(assets, "fonts/glegoo_regular.ttf");

        ((TextView) findViewById(R.id.app_name_tv))
                .setTypeface(Typeface.createFromAsset(assets, "fonts/edo.ttf"));
        ((TextView) findViewById(R.id.rounds_selected_tv))
                .setTypeface(glegoo);

        TextView roundsSelectedNumberTV = findViewById(R.id.rounds_selected_number_tv);
        roundsSelectedNumberTV.setTypeface(glegoo);

        Button saveButton = findViewById(R.id.save_button),
                cancelButton = findViewById(R.id.cancel_button);
        saveButton.setTypeface(schoolbell);
        saveButton.setOnClickListener(this);
        cancelButton.setTypeface(schoolbell);
        cancelButton.setOnClickListener(this);

        roundsContainer = findViewById(R.id.rounds_container);
        roundsContainer.setWeightSum(MAX_ROUNDS);

        LayoutInflater inflater = getLayoutInflater();

        Intent intent = getIntent();
        int roundsCount = intent.getIntExtra("roundsCount", 3);
        int currentRound = intent.getIntExtra("currentRound", 0);
        int[] roundsResults = intent.getIntArrayExtra("roundsResults");
        roundsSelectedNumberTV.setText(String.format("%s", roundsCount));
        for (int i = 0; i < roundsCount; i++) {
            View view = inflater.inflate(R.layout.round_vh, roundsContainer, false);

            TextView roundNTV = view.findViewById(R.id.round_n_tv);
            roundNTV.setTypeface(glegoo);
            roundNTV.setText(getString(R.string.round_n).replace("N", String.format("%s", i + 1)));

            TextView roundNPlayerTV = view.findViewById(R.id.round_n_player_tv);
            roundNPlayerTV.setTypeface(glegoo);

            if (i <= currentRound && roundsResults[i] != GameActivity.TAG_DRAW) {
                View rewardView = view.findViewById(R.id.reward_view);
                rewardView.setVisibility(VISIBLE);
                rewardView.animate().alpha(1f).start();
            }
            roundNPlayerTV.setText(i == currentRound + 1 ?
                    getString(R.string.ongoing) :
                    i <= currentRound ?
                            roundsResults[i] == GameActivity.TAG_DRAW ?
                                    getString(R.string.draw) :
                                    getString(R.string.player_n)
                                            .replace("N", String.format("%s", roundsResults[i] + 1))
                                            .replace(":", "") : "");

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(WRAP_CONTENT, 0);
            layoutParams.weight = 1;
            layoutParams.bottomMargin = 0;
            layoutParams.topMargin = 0;
            roundsContainer.addView(view, layoutParams);
        }

        HashMap<Integer, Integer> players = new HashMap<>();
        for (int current : roundsResults)
            if (current != TAG_EMPTY && current != TAG_DRAW)
                if (!players.containsKey(current))
                    players.put(current, 0);
                else {
                    int rounds = players.get(current);
                    players.remove(current);
                    players.put(current, rounds + 1);
                }

        int winner = TAG_DRAW, maxRounds = 0;
        if (players.size() != 0)
            for (int player : players.keySet()) {
                int rounds = players.get(player);
                if (rounds > maxRounds) {
                    winner = player;
                    maxRounds = rounds;
                }
            }

        final int ourWinner = winner;
        if (currentRound == roundsCount - 1)
            Executors.newSingleThreadExecutor()
                    .execute(() ->
                    {
                        try {
                            cancelButton.setOnClickListener(this);
                            Thread.sleep(2000);
                            startActivity(new Intent(OverviewActivity.this, RewardActivity.class)
                                    .putExtra("playerName", ourWinner == TAG_DRAW ? getString(R.string.nobody) :

                                            ourWinner == intent.getIntExtra("playersCount", 2) - 1 &&
                                                    intent.getBooleanExtra("withBot", false) ?

                                                    getString(R.string.bot) :
                                                    String.format("Player %s", ourWinner + 1)));
                        } catch (InterruptedException e) {
                        }
                    });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_button:
                setResult(GameActivity.SAVE_RESULT);
                onBackPressed();
                break;
            case R.id.cancel_button:
                onBackPressed();
                break;
        }
    }
}
