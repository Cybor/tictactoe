package com.anndefined.cybor.tictacline.data;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private int tag = -1;
    private List<Integer> cells = new ArrayList<>();

    public Player(int tag) {
        this.tag = tag;
    }

    public int getTag() {
        return tag;
    }

    public Player setTag(int tag) {
        this.tag = tag;
        return this;
    }

    public List<Integer> getCells() {
        return cells;
    }

    public Player setCells(List<Integer> cells) {
        this.cells = cells;
        return this;
    }
}
