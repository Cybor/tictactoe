package com.anndefined.cybor.tictacline.activities;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anndefined.cybor.tictacline.R;

import java.util.concurrent.Executors;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.anndefined.cybor.tictacline.activities.GameActivity.SAVE_RESULT;

public class ScoreboardActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int EMPTY = -1;
    private final int MAX_PLAYERS = 6;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scoreboard_layout);

        AssetManager assets = getAssets();
        Typeface glegoo = Typeface.createFromAsset(assets, "fonts/glegoo_regular.ttf");
        Typeface edo = Typeface.createFromAsset(assets, "fonts/edo.ttf");

        ((TextView) findViewById(R.id.app_name_tv)).setTypeface(edo);
        ((TextView) findViewById(R.id.current_round_tv)).setTypeface(glegoo);

        TextView closeButton = findViewById(R.id.close_button);
        closeButton.setTypeface(edo);
        closeButton.setOnClickListener(this);


        TextView currentRoundNumberTV = findViewById(R.id.current_round_number_tv);
        currentRoundNumberTV.setTypeface(glegoo);

        TextView overviewButton = findViewById(R.id.overview_button);
        overviewButton.setTypeface(glegoo);
        overviewButton.setOnClickListener(this);

        View backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(this);

        LinearLayout playersContainer = findViewById(R.id.players_container);
        playersContainer.setWeightSum(MAX_PLAYERS);
        LayoutInflater inflater = getLayoutInflater();

        Intent intent = getIntent();
        int[] data = getIntent().getIntArrayExtra("scores");
        currentRoundNumberTV.setText(String.format("%s", intent.getIntExtra("currentRound", 0) + 1));

        for (int i = 0; i < data.length; i++) {
            View view = inflater.inflate(R.layout.player_vh, playersContainer, false);

            TextView playerNTV = view.findViewById(R.id.player_n_tv);
            playerNTV.setTypeface(glegoo);
            playerNTV.setText(getString(R.string.player_n).replace("N", String.format("%s", i + 1)));

            TextView playerNScoreTV = view.findViewById(R.id.player_n_score_tv);
            playerNScoreTV.setTypeface(glegoo);
            playerNScoreTV.setText(String.format("%s", data[i]));

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(WRAP_CONTENT, 0);
            layoutParams.weight = 1;
            layoutParams.bottomMargin = 0;
            layoutParams.topMargin = 0;
            playersContainer.addView(view, layoutParams);
        }

        if (intent.getIntExtra("currentRound", 0) == intent.getIntExtra("roundsResults", 3) - 1)
            Executors.newSingleThreadExecutor()
                    .execute(() ->
                    {
                        try {
                            backButton.setOnClickListener(null);
                            Thread.sleep(2000);
                            startActivity(new Intent(ScoreboardActivity.this, OverviewActivity.class)
                                    .putExtras(intent));
                        } catch (InterruptedException e) {
                        }
                    });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.overview_button:
                startActivityForResult(new Intent(this, OverviewActivity.class)
                        .putExtras(getIntent()), 0);
                break;
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.close_button:
                finish();
                break;
            case R.id.save_button:
                setResult(SAVE_RESULT);
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == SAVE_RESULT) {
            setResult(resultCode);
            onBackPressed();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
