package com.anndefined.cybor.tictacline.activities;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.anndefined.cybor.tictacline.R;
import com.anndefined.cybor.tictacline.data.Settings;
import com.anndefined.cybor.tictacline.views.LabeledButton;

public class SettingsActivity extends Activity implements View.OnClickListener {
    private LabeledButton musicButton, vibrationButton, autoSavingButton;
    private Settings settings;
    private String onString, offString;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);

        ((TextView) findViewById(R.id.app_name_tv))
                .setTypeface(Typeface.createFromAsset(getAssets(), "fonts/edo.ttf"));
        ((TextView) findViewById(R.id.screen_header_tv))
                .setTypeface(Typeface.createFromAsset(getAssets(), "fonts/schoolbell.ttf"));

        onString = getString(R.string.on);
        offString = getString(R.string.off);

        settings = Settings.getInstance(this);

        (musicButton = findViewById(R.id.music_button))
                .setContent(settings.isMusicOn() ? onString : offString)
                .setOnClickListener(this);
        (vibrationButton = findViewById(R.id.vibration_button))
                .setContent(settings.isVibrationOn() ? onString : offString)
                .setOnClickListener(this);
        (autoSavingButton = findViewById(R.id.auto_saving_button))
                .setContent(settings.isAutoSavingOn() ? onString : offString)
                .setOnClickListener(this);

        TextView closeButton = findViewById(R.id.close_button);
        closeButton.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/edo.ttf"));
        closeButton.setOnClickListener(this);

        findViewById(R.id.back_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.music_button:
                boolean musicOn = !settings.isMusicOn();
                settings.setMusicOn(musicOn);
                musicButton.setContent(musicOn ? onString : offString);
                break;
            case R.id.vibration_button:
                boolean vibrationOn = !settings.isVibrationOn();
                settings.setVibrationOn(vibrationOn);
                if (vibrationOn)
                    //noinspection deprecation
                    ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(300);
                vibrationButton.setContent(vibrationOn ? onString : offString);
                break;
            case R.id.auto_saving_button:
                boolean autoSavingOn = !settings.isAutoSavingOn();
                settings.setAutoSavingOn(autoSavingOn);
                autoSavingButton.setContent(autoSavingOn ? onString : offString);
                break;
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.close_button:
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        settings.save();
        super.onBackPressed();
    }
}
