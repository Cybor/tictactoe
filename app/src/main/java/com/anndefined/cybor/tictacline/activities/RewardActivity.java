package com.anndefined.cybor.tictacline.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.anndefined.cybor.tictacline.R;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

public class RewardActivity extends Activity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reward_layout);

        ((TextView) findViewById(R.id.screen_header_tv))
                .setText(getString(R.string.congratulations)
                        .replace("PLAYER_NAME",
                                getIntent().getStringExtra("playerName")));

        findViewById(R.id.screen_header_tv).setOnClickListener(this);
        findViewById(R.id.back_button).setOnClickListener(this);
        findViewById(R.id.close_button).setOnClickListener(this);
        findViewById(R.id.screenshot_button).setOnClickListener(this);
        findViewById(R.id.share_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.close_button:
                finish();
                break;
            case R.id.screenshot_button:
                takeScreenshot();
                break;
            case R.id.share_button:
                File screenshotFile = takeScreenshot();
                if (screenshotFile != null)
                    shareScreenshot(screenshotFile);
                break;
        }
    }

    private File takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss_S", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();

            openScreenshot(imageFile);

            return imageFile;
        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
            return null;
        }
    }

    private void openScreenshot(File imageFile) {
        startActivity(new Intent()
                .setAction(Intent.ACTION_VIEW)
                .setDataAndType(Uri.fromFile(imageFile), "image/*"));
    }

    private void shareScreenshot(File imageFile) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "title");
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");

        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));
        startActivity(Intent.createChooser(share, "Share Image"));
    }

}
