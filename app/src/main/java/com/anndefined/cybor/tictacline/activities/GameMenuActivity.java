package com.anndefined.cybor.tictacline.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.anndefined.cybor.tictacline.R;

import static android.widget.Toast.LENGTH_LONG;

public class GameMenuActivity extends Activity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_menu_layout);

        setButtons(getApplicationContext().getAssets(),
                R.id.app_name_tv, "fonts/edo.ttf",
                R.id.close_button, "fonts/edo.ttf",
                R.id.play_button, "fonts/schoolbell.ttf",
                R.id.load_button, "fonts/schoolbell.ttf",
                R.id.levels_button, "fonts/schoolbell.ttf",
                R.id.settings_button, "fonts/schoolbell.ttf"
        );

    }

    private void setButtons(AssetManager assetManager, Object... buttonFontPairs) {
        for (int i = 0; i < buttonFontPairs.length; i += 2) {
            TextView view = findViewById((int) buttonFontPairs[i]);
            view.setTypeface(Typeface.createFromAsset(assetManager, (String) buttonFontPairs[i + 1]));
            view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.app_name_tv:
                Toast.makeText(this, getString(R.string.copyright).replace("\\n", "\n"), LENGTH_LONG).show();
                break;
            case R.id.close_button:
                finish();
                break;
            case R.id.play_button:
                startActivity(new Intent(this, PreGameActivity.class).putExtra("newGame", true));
                break;
            case R.id.load_button:
                startActivity(new Intent(this, GameActivity.class).putExtra("newGame", false));
                break;
            case R.id.levels_button:
                startActivity(new Intent(this, LevelsActivity.class));
                break;
            case R.id.settings_button:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }
    }
}
