package com.anndefined.cybor.tictacline.views;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.anndefined.cybor.tictacline.R;

import static android.graphics.Color.BLACK;
import static android.util.TypedValue.COMPLEX_UNIT_DIP;
import static android.util.TypedValue.applyDimension;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class LabeledButton extends FrameLayout {

    private TextView labelTV, contentTV;

    public LabeledButton(Context context) {
        super(context);
        initViews();
    }

    public LabeledButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initViews(context.obtainStyledAttributes(attrs, R.styleable.LabeledButton));
    }

    public LabeledButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context.obtainStyledAttributes(attrs, R.styleable.LabeledButton, defStyleAttr, 0));
    }

    private void initViews() {
        initViews(null);
    }

    private void initViews(@Nullable TypedArray attrs) {
        View view = inflate(getContext(), R.layout.labeled_button_layout, this);
        AssetManager am = getContext().getApplicationContext().getAssets();
        labelTV = view.findViewById(R.id.label_tv);
        contentTV = view.findViewById(R.id.content_tv);
        if (attrs != null) {
            labelTV.setText(attrs.getText(R.styleable.LabeledButton_labelText));
            contentTV.setText(attrs.getText(R.styleable.LabeledButton_contentText));
            //if (!isInEditMode()) {
            labelTV.setTypeface(Typeface.createFromAsset(am, "fonts/glegoo_regular.ttf"));
            contentTV.setTypeface(Typeface.createFromAsset(am, "fonts/glegoo_bold.ttf"));
            //}
        }

        ((FrameLayout) view.findViewById(R.id.content_container))
                .addView(new View(getContext()) {
                    @Override
                    public void draw(Canvas canvas) {
                        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                        paint.setColor(BLACK);
                        DisplayMetrics metrics = getResources().getDisplayMetrics();
                        paint.setStrokeWidth(applyDimension(COMPLEX_UNIT_DIP, 2, metrics));
                        int width = getWidth();
                        int height = getHeight();
                        int labelWidth = labelTV.getWidth();

                        canvas.drawLine(0, 0, 0, height, paint);
                        canvas.drawLine(0, height, width, height, paint);
                        canvas.drawLine(width, height, width, 0, paint);

                        canvas.drawLine(0, 0, width / 2 - labelWidth / 2, 0, paint);
                        canvas.drawLine(width / 2 + labelWidth / 2, 0, width, 0, paint);

                        super.draw(canvas);
                    }
                }, MATCH_PARENT, WRAP_CONTENT);
    }

    public LabeledButton setLabel(String label) {
        labelTV.setText(label);
        return this;
    }

    public LabeledButton setContent(String content) {
        contentTV.setText(content);
        return this;
    }
}
