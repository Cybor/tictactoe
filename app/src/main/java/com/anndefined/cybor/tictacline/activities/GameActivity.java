package com.anndefined.cybor.tictacline.activities;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anndefined.cybor.tictacline.R;
import com.anndefined.cybor.tictacline.data.GameState;
import com.anndefined.cybor.tictacline.data.Player;
import com.anndefined.cybor.tictacline.data.Settings;

import java.io.IOException;
import java.util.List;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
import static android.graphics.Color.RED;
import static android.util.TypedValue.COMPLEX_UNIT_DIP;
import static android.util.TypedValue.applyDimension;
import static android.widget.Toast.LENGTH_LONG;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int TAG_EMPTY = -1, TAG_DRAW = -2;
    public static final int SAVE_RESULT = -1, NOTHING_RESULT = 0;
    private static final int BASE_SCORE = 10, SCORE_STEP_CELL = 3;
    private static GameState gameState;
    private static Settings settings;
    private FrameLayout gameContainer;
    private MediaPlayer mediaPlayer;
    private float cellWidth = 0, cellHeight = 0, cellButtonSize = 0,
            paddingHorizontal = 0, paddingVertical = 0;
    private int fieldSize = 3;
    private float padding = 10;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setRequestedOrientation(SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_layout);

        padding = getResources().getDimension(R.dimen.game_cells_between_margin);

        Typeface edo = Typeface.createFromAsset(getAssets(), "fonts/edo.ttf");

        ((TextView) findViewById(R.id.app_name_tv)).setTypeface(edo);

        TextView closeButton = findViewById(R.id.close_button);
        closeButton.setTypeface(edo);
        closeButton.setOnClickListener(this);

        gameState = GameState.getInstance(this);

        boolean newGame = getIntent().getBooleanExtra("newGame", true);
        if (newGame) newGame();
        else {
            settings = gameState.getSettings();
            fieldSize = settings.getFieldSize();
        }

        gameState.setGameLocked(false);
        gameState.setPlayerIndex(0);


        int playersCount = gameState.getPlayersCount();
        if (playersCount > fieldSize - 1) {
            Toast.makeText(this,
                    getString(R.string.too_small_field)
                            .replace("SIZE", String.format("%1$sx%1$s", fieldSize))
                            .replace("PLAYERS", String.format("%s", playersCount)),
                    LENGTH_LONG).show();
            settings.setFieldSize((byte) (fieldSize = playersCount + 1));
            settings.save();
        }

        if (settings.isMusicOn())
            playBackgroundMusic();

        findViewById(R.id.app_name_tv).setOnClickListener(this);
        findViewById(R.id.back_button).setOnClickListener(this);
        findViewById(R.id.close_button).setOnClickListener(this);
        findViewById(R.id.save_button).setOnClickListener(this);

        gameContainer = findViewById(R.id.game_container);
        gameContainer.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft,
                                       int oldTop, int oldRight, int oldBottom) {
                cellWidth = (v.getWidth() - 2 * fieldSize * padding - padding) / fieldSize + padding;
                cellHeight = (v.getHeight() - 2 * fieldSize * padding - padding) / fieldSize;// + PADDING;
                gameContainer.setPadding((int) padding, 0, 0, 0);

                int[] cellValues = gameState.getCellValues();
                for (int y = 0; y < fieldSize; y++)
                    for (int x = 0; x < fieldSize; x++) {
                        Button button = new Button(GameActivity.this);

                        button.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/edo.ttf"));
                        if (newGame)
                            button.setTag(TAG_EMPTY);
                        else {
                            int tag = cellValues[coordsToIndex(x, y)];
                            button.setTag(tag);
                            if (tag != TAG_EMPTY)
                                button.setText(String.format("%s", tag + 1));
                        }


                        if (cellWidth < cellHeight) {
                            cellButtonSize = cellWidth;
                            paddingVertical = padding + (cellHeight - cellWidth);
                            paddingHorizontal = padding;
                        } else {
                            cellButtonSize = cellHeight;
                            paddingVertical = padding;
                            paddingHorizontal = padding + (cellWidth - cellHeight);
                        }

                        gameContainer.addView(button, (int) cellButtonSize, (int) cellButtonSize);

                        ViewPropertyAnimator animator = button.animate()
                                .translationX(x * cellButtonSize + (x == 0 ? 0 : paddingHorizontal * x))
                                .translationY(y * cellButtonSize + (y == 0 ? 0 : paddingVertical * y))
                                .setStartDelay(coordsToIndex(x, y) * 100);
                        if (x >= fieldSize - 1 && y >= fieldSize - 1)
                            animator.withEndAction(() -> gameState.setGameLocked(false));
                        animator.start();
                        button.setOnClickListener(GameActivity.this);
                    }
                gameContainer.removeOnLayoutChangeListener(this);
            }
        });
    }

    private void newGame() {
        Intent intent = getIntent();

        gameState.remove();
        gameState = GameState.getInstance(this);

        try {
            settings = (Settings) Settings.getInstance(this).clone();
        } catch (CloneNotSupportedException e) {
            Toast.makeText(this, "ERROR", LENGTH_LONG).show();
        }
        gameState.setSettings(settings);

        int playersCount = intent.getIntExtra("playersCount", 2);
        if (playersCount == 1) {
            playersCount = 2;
            gameState.setWithBot(true);
        }

        gameState.setPlayersCount(playersCount);

        int[] scores = new int[playersCount];
        for (int i = 0; i < scores.length; i++) scores[i] = 0;
        gameState.setScores(scores);

        int roundsCount = intent.getIntExtra("roundsCount", 3);
        gameState.setRoundsCount(roundsCount);
        int[] roundsResults = new int[roundsCount];
        for (int i = 0; i < roundsResults.length; i++)
            roundsResults[i] = TAG_EMPTY;
        gameState.setRoundsResults(roundsResults);

        fieldSize = settings.getFieldSize();
        gameState.setCellValues(new int[(int) Math.pow(fieldSize, 2)]);
    }

    private void playBackgroundMusic() {
        try {
            AssetFileDescriptor fileDescriptor = getAssets().openFd("audio/music.mp3");
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(fileDescriptor.getFileDescriptor(),
                    fileDescriptor.getStartOffset(),
                    fileDescriptor.getLength());
            mediaPlayer.setLooping(true);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            Log.e("Game", "playBackgroundMusic", e);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.close_button:
                finish();
                break;
            case R.id.save_button:
                saveGame();
                break;
            default:
                if (!gameState.isGameLocked())
                    if (move((Button) view) && gameState.isWithBot() && !gameState.isGameLocked())
                        botMove(gameState.getPlayersCount() - 1);
                break;
        }
    }

    private void botMove(int tagBot) {

        for (int x = 0; x < fieldSize - 1; x++)
            for (int y = 0; y < fieldSize - 1; y++) {
                int tag = getValue(x, y);
                if (tag != TAG_EMPTY /*&& tag == tagBot*/)
                    if (tag == getValue(x + 1, y)) {
                        if (x + 2 < fieldSize && getValue(x + 2, y) == TAG_EMPTY) {
                            move(x + 2, y, tagBot);
                            return;
                        } else if (x > 0 && getValue(x - 1, y) == TAG_EMPTY) {
                            move(x - 1, y, tagBot);
                            return;
                        }
                    } else if (tag == getValue(x, y + 1)) {
                        if (y + 2 < fieldSize && getValue(x, y + 2) == TAG_EMPTY) {
                            move(x, y + 2, tagBot);
                            return;
                        } else if (y > 0 && getValue(x, y - 1) == TAG_EMPTY) {
                            move(x, y - 1, tagBot);
                            return;
                        }
                    } else if (tag == getValue(x + 1, y + 1)) {
                        if (x + 2 < fieldSize && y + 2 < fieldSize && getValue(x + 2, y + 2) == TAG_EMPTY) {
                            move(x + 2, y + 2, tagBot);
                            return;
                        } else if (x > 0 && y > 0 && getValue(x - 1, y - 1) == TAG_EMPTY) {
                            move(x - 1, y - 1, tagBot);
                            return;
                        }
                    }
            }
        for (int x = 0; x < fieldSize; x++)
            for (int y = 0; y < fieldSize; y++)
                if (getValue(x, y) == TAG_EMPTY) {
                    move(x, y, tagBot);
                    return;
                }
    }

    private void move(int x, int y, int playerIndex) {
        move((Button) gameContainer.getChildAt(coordsToIndex(x, y)), playerIndex);
    }

    private boolean move(Button button) {
        return move(button, gameState.getPlayerIndex());
    }

    private boolean move(Button button, int playerIndex) {
        int tagSource = (int) button.getTag();
        if (tagSource == TAG_EMPTY || settings.isSwitchable()) {

            button.setText(String.format("%s", playerIndex + 1));

            button.setTag(playerIndex);
            Player winner;
            if ((winner = tryMove(tagSource, playerIndex)).getTag() != TAG_EMPTY) reportWin(winner);
            if (playerIndex >= gameState.getPlayersCount() - 1) gameState.setPlayerIndex(0);
            else gameState.setPlayerIndex(playerIndex + 1);

            return true;
        }
        return false;

    }

    private Player tryMove(int tagSource, int tag) {
        Player player = new Player(TAG_EMPTY);

        int moveCount = gameState.getMoveCount();
        if (tagSource == TAG_EMPTY) gameState.setMoveCount(++moveCount);

        if (moveCount >= 3)
            for (int frameX = 0; frameX <= fieldSize - 3; frameX++)
                for (int frameY = 0; frameY <= fieldSize - 3; frameY++)
                    if (checkRow(tag, player, frameX, frameY) ||
                            checkCol(tag, player, frameX, frameY) ||
                            checkDiag(tag, player, frameX, frameY) ||
                            checkAntiDiag(tag, player, frameX, frameY))
                        return player.setTag(tag);

        //check draw
        if (moveCount == Math.pow(fieldSize, 2))
            return player.setTag(TAG_DRAW);

        return player;
    }

    private boolean checkAntiDiag(int tag, Player player, int frameX, int frameY) {
        int sameAccumulator = 0;
        player.getCells().clear();
        for (int xy = 0; xy < 3; xy++) {
            int value = getValue(xy + frameX, (fieldSize - 1) - (xy + frameY));
            if (value != TAG_EMPTY)
                if (value != tag) break;
                else {
                    sameAccumulator++;
                    player.getCells().add(coordsToIndex(xy + frameX, (fieldSize - 1) - (xy + frameY)));
                }
            if (sameAccumulator >= 3)
                return true;
        }
        return false;
    }

    private boolean checkDiag(int tag, Player player, int frameX, int frameY) {
        int sameAccumulator = 0;
        player.getCells().clear();
        for (int xy = 0; xy < 3; xy++) {
            int value = getValue(xy + frameX, xy + frameY);
            if (value != TAG_EMPTY)
                if (value != tag) break;
                else {
                    sameAccumulator++;
                    player.getCells().add(coordsToIndex(xy + frameX, xy + frameY));
                }
            if (sameAccumulator >= 3)
                return true;
        }
        return false;
    }

    private boolean checkCol(int tag, Player player, int frameX, int frameY) {
        int sameAccumulator;
        for (int y = 0; y < 3; y++) {
            sameAccumulator = 0;
            player.getCells().clear();
            for (int x = 0; x < 3; x++) {
                int value = getValue(x + frameX, y + frameY);
                if (value != TAG_EMPTY)
                    if (value != tag) break;
                    else {
                        sameAccumulator++;
                        player.getCells().add(coordsToIndex(x + frameX, y + frameY));
                    }
                if (sameAccumulator >= 3)
                    return true;
            }
        }
        return false;
    }

    private boolean checkRow(int tag, Player player, int frameX, int frameY) {
        int sameAccumulator;
        for (int x = 0; x < 3; x++) {
            sameAccumulator = 0;
            player.getCells().clear();
            for (int y = 0; y < 3; y++) {
                int value = getValue(x + frameX, y + frameY);
                if (value != TAG_EMPTY)
                    if (value != tag) break;
                    else {
                        sameAccumulator++;
                        player.getCells().add(coordsToIndex(x + frameX, y + frameY));
                    }
                if (sameAccumulator >= 3)
                    return true;
            }
        }
        return false;
    }

    private void reportWin(Player player) {
        Intent scoreboardIntent = new Intent(this, ScoreboardActivity.class)
                .putExtra("roundsCount", gameState.getRoundsCount())
                .putExtra("scores", gameState.getScores())
                .putExtra("roundsResults", gameState.getRoundsResults())
                .putExtra("currentRound", gameState.getCurrentRound())
                .putExtra("playersCount", gameState.getPlayersCount())
                .putExtra("withBot", gameState.isWithBot());

        switch (player.getTag()) {
            case TAG_DRAW:
                gameState.setGameLocked(true);
                gameState.getRoundsResults()[gameState.getCurrentRound()] = TAG_DRAW;
                startActivityForResult(scoreboardIntent, 0);
                nextRound();
                break;
            case TAG_EMPTY:
                break;
            default:
                gameState.setGameLocked(true);

                List<Integer> cells = player.getCells();
                View firstCell = gameContainer.getChildAt(cells.get(0));
                View lastCell = gameContainer.getChildAt(cells.get(cells.size() - 1));

                float firstX = (int) firstCell.getTranslationX() + padding + cellButtonSize / 2,
                        firstY = (int) firstCell.getTranslationY() + padding + cellButtonSize / 2,
                        lastX = (int) lastCell.getTranslationX() + padding + cellButtonSize / 2,
                        lastY = (int) lastCell.getTranslationY() + padding + cellButtonSize / 2,
                        width = Math.abs(lastX - firstX),
                        height = Math.abs(lastY - firstY);
                boolean revX = firstX > lastX;
                boolean revY = firstY > lastY;

                View line = new View(this) {
                    @Override
                    public void draw(Canvas canvas) {
                        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                        paint.setColor(RED);
                        paint.setStrokeWidth(applyDimension(COMPLEX_UNIT_DIP, 3, getResources().getDisplayMetrics()));
                        int width = getWidth();
                        int height = getHeight();
                        canvas.drawLine(revX ? width : 0, revY ? height : 0, revX ? 0 : width, revY ? 0 : height, paint);
                        super.draw(canvas);
                    }
                };
                line.setTranslationX(revX ? lastX : firstX);
                line.setTranslationY(revY ? lastY : firstY);


                DisplayMetrics metrics = getResources().getDisplayMetrics();
                gameContainer.addView(line,
                        (int) (width == 0 ? applyDimension(COMPLEX_UNIT_DIP, 10, metrics) : width),
                        (int) (height == 0 ? applyDimension(COMPLEX_UNIT_DIP, 10, metrics) : height));

                for (int i = 0; i < cells.size(); i++) {
                    View cell = gameContainer.getChildAt(cells.get(i));
                    final int index = i;
                    cell.animate().alpha(0f).withEndAction(() ->
                    {
                        ViewPropertyAnimator animator = cell.animate().alpha(1f);
                        if (index >= cells.size() - 1)
                            animator.withEndAction(() ->
                            {
                                gameContainer.removeView(line);
                                gameState.getScores()[player.getTag()] += gameState.getCurrentRound() == 0 ?
                                        BASE_SCORE : SCORE_STEP_CELL * player.getCells().size();
                                gameState.getRoundsResults()[gameState.getCurrentRound()] = player.getTag();
                                startActivityForResult(scoreboardIntent, 0);
                                nextRound();
                            });
                        animator.start();
                    }).setStartDelay(0).start();
                }
                break;
        }
    }

    private void nextRound() {
        int currentRound = gameState.getCurrentRound();
        if (++currentRound == gameState.getRoundsCount()) currentRound = 0;
        gameState.setCurrentRound(currentRound);
        for (int i = 0; i < gameContainer.getChildCount(); i++) {
            TextView view = (TextView) gameContainer.getChildAt(i);
            view.setTag(TAG_EMPTY);
            view.setText("");
        }
        gameState.setMoveCount(0);
        gameState.setPlayerIndex(0);
        gameState.setGameLocked(false);
    }

    private int getValue(int x, int y) {
        return (int) gameContainer.getChildAt(coordsToIndex(x, y)).getTag();
    }

    private int coordsToIndex(int c1, int c2) {
        return c1 + c2 * fieldSize;
    }

    private void saveGame() {
        int[] cellValues = gameState.getCellValues();
        for (int i = 0; i < gameContainer.getChildCount(); i++)
            cellValues[i] = (int) gameContainer.getChildAt(i).getTag();
        gameState.save();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == SAVE_RESULT) saveGame();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStop() {
        if (mediaPlayer != null && mediaPlayer.isPlaying())
            mediaPlayer.stop();
        super.onStop();
    }
}