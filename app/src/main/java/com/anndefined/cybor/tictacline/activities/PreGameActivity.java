package com.anndefined.cybor.tictacline.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewParent;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anndefined.cybor.tictacline.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class PreGameActivity extends Activity
        implements View.OnClickListener {
    private LinearLayout playersSelectionContainer1, playersSelectionContainer2, playersSelectionContainerRoot, roundSelectionContainer;
    private int playersCount = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pre_game_layout);

        ((TextView) findViewById(R.id.app_name_tv))
                .setTypeface(Typeface.createFromAsset(getAssets(), "fonts/edo.ttf"));

        AssetManager assetManager = getApplicationContext().getAssets();
        TextView closeButton = findViewById(R.id.close_button);
        closeButton.setTypeface(Typeface.createFromAsset(assetManager, "fonts/edo.ttf"));
        closeButton.setOnClickListener(this);

        playersSelectionContainer1 = findViewById(R.id.players_selection_container1);
        playersSelectionContainer2 = findViewById(R.id.players_selection_container2);
        playersSelectionContainerRoot = findViewById(R.id.players_selection_container_root);
        roundSelectionContainer = findViewById(R.id.rounds_selection_container);

        findViewById(R.id.back_button).setOnClickListener(this);

        playersSelectionContainerRoot.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft,
                                       int oldTop, int oldRight, int oldBottom) {

                int playersContainerViewCount = playersSelectionContainer1.getChildCount() +
                        playersSelectionContainer2.getChildCount();
                int roundsContainerViewCount = roundSelectionContainer.getChildCount();
                Typeface schoolBell = Typeface.createFromAsset(assetManager, "fonts/schoolbell.ttf");
                int preGameItemsBetweenMargin
                        = (int) getResources().getDimension(R.dimen.pre_game_items_between_margin);
                int playersItemSize = v.getWidth() / 3 - preGameItemsBetweenMargin;
                int roundsItemSize = v.getWidth() / 2 - preGameItemsBetweenMargin;

                for (int i = 0; i < playersContainerViewCount; i++) {
                    TextView playersTV;
                    if (i < playersContainerViewCount / 2)
                        playersTV = (TextView) playersSelectionContainer1.getChildAt(i);
                    else
                        playersTV = (TextView) playersSelectionContainer2.getChildAt(playersContainerViewCount - i - 1);
                    playersTV.setOnClickListener(PreGameActivity.this);
                    playersTV.setTypeface(schoolBell);


                    LinearLayout.LayoutParams playersTVLayoutParams
                            = (LinearLayout.LayoutParams) playersTV.getLayoutParams();
                    playersTVLayoutParams.width = playersItemSize;
                    playersTVLayoutParams.height = playersItemSize;

                    if (i < roundsContainerViewCount) {
                        TextView roundsTV = (TextView) roundSelectionContainer.getChildAt(i);
                        roundsTV.setOnClickListener(PreGameActivity.this);
                        roundsTV.setTypeface(schoolBell);

                        LinearLayout.LayoutParams roundsTVLayoutParams
                                = (LinearLayout.LayoutParams) roundsTV.getLayoutParams();
                        roundsTVLayoutParams.width = roundsItemSize;
                        roundsTVLayoutParams.height = roundsItemSize;
                    }
                }
                playersSelectionContainerRoot.removeOnLayoutChangeListener(this);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.close_button:
                finish();
                break;
            default:
                ViewParent parent = view.getParent();
                if (parent == playersSelectionContainer1 ||
                        parent == playersSelectionContainer2) {
                    playersCount = Integer.parseInt(view.getTag().toString());
                    playersSelectionContainerRoot.setVisibility(GONE);
                    roundSelectionContainer.setVisibility(VISIBLE);
                    ((TextView) findViewById(R.id.screen_header_tv)).setText(R.string.rounds);
                } else if (parent == roundSelectionContainer) {
                    startActivity(new Intent(this, GameActivity.class)
                            .putExtra("playersCount", playersCount)
                            .putExtra("roundsCount", Integer.parseInt(view.getTag().toString()))
                            .putExtras(getIntent()));
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (playersSelectionContainerRoot.getVisibility() == GONE) {
            roundSelectionContainer.setVisibility(GONE);
            playersSelectionContainerRoot.setVisibility(VISIBLE);
        } else super.onBackPressed();
    }
}
