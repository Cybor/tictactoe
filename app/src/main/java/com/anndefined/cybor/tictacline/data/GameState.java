package com.anndefined.cybor.tictacline.data;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class GameState {
    private static transient GameState instance;
    private static transient File gameStateFile;
    private static transient Gson gson;

    private Settings settings;
    private boolean gameLocked = true, withBot = false;
    private int playerIndex = 0, currentRound = 0;
    private int playersCount = 2, roundsCount = 3, moveCount = 0;
    private int[] scores, roundsResults, cellValues;

    private GameState() {
    }

    public static GameState getInstance(Context context) {
        if (instance != null) return instance;
        else {
            gson = new GsonBuilder().serializeNulls().create();
            gameStateFile = new File(context.getApplicationInfo().dataDir, "gameState.json");
            try {
                if (gameStateFile.exists()) {
                    FileReader reader = new FileReader(gameStateFile);
                    return (instance = gson.fromJson(reader, GameState.class));
                }
            } catch (IOException e) {
                Log.e("GameState", "Reading", e);
            }
            return (instance = new GameState());
        }
    }

    public void save() {
        try {
            FileWriter writer = new FileWriter(gameStateFile);
            if (gameStateFile.exists() || gameStateFile.createNewFile()) {
                writer.write(gson.toJson(this));
                writer.close();
            }
        } catch (IOException e) {
            Log.e("GameState", "Saving", e);
        }
    }

    public void remove() {
        instance = null;
        if (!gameStateFile.delete())
            Log.wtf("GameState", "Couldn't remove unexist  ");
    }


    //region Getters-setters
    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public boolean isGameLocked() {
        return gameLocked;
    }

    public void setGameLocked(boolean gameLocked) {
        this.gameLocked = gameLocked;
    }

    public int getPlayerIndex() {
        return playerIndex;
    }

    public void setPlayerIndex(int playerIndex) {
        this.playerIndex = playerIndex;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public int getPlayersCount() {
        return playersCount;
    }

    public void setPlayersCount(int playersCount) {
        this.playersCount = playersCount;
    }

    public int getRoundsCount() {
        return roundsCount;
    }

    public void setRoundsCount(int roundsCount) {
        this.roundsCount = roundsCount;
    }

    public int getMoveCount() {
        return moveCount;
    }

    public void setMoveCount(int moveCount) {
        this.moveCount = moveCount;
    }

    public int[] getScores() {
        return scores;
    }

    public void setScores(int[] scores) {
        this.scores = scores;
    }

    public int[] getRoundsResults() {
        return roundsResults;
    }

    public void setRoundsResults(int[] roundsResults) {
        this.roundsResults = roundsResults;
    }

    public int[] getCellValues() {
        return cellValues;
    }

    public void setCellValues(int[] cellValues) {
        this.cellValues = cellValues;
    }

    public boolean isWithBot() {
        return withBot;
    }

    public void setWithBot(boolean withBot) {
        this.withBot = withBot;
    }

    //endregion
}
