package com.anndefined.cybor.tictacline.data;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Settings implements Cloneable {
    private static transient Settings instance;
    private static transient File settingsFile;
    private static transient Gson gson;

    private byte fieldSize = 3;
    private boolean switchable = false;
    private boolean musicOn = true;
    private boolean vibrationOn = true;
    private boolean autoSavingOn = true;

    public Settings() {
    }

    public static Settings getInstance(Context context) {
        if (instance != null) return instance;
        else {
            gson = new GsonBuilder().serializeNulls().create();
            settingsFile = new File(context.getApplicationInfo().dataDir, "settings.json");
            try {
                if (settingsFile.exists()) {
                    FileReader reader = new FileReader(settingsFile);
                    return (instance = gson.fromJson(reader, Settings.class));
                }
            } catch (IOException e) {
                Log.e("Settings", "Reading", e);
            }
            return (instance = new Settings());
        }
    }

    public byte getFieldSize() {
        return fieldSize;
    }

    public Settings setFieldSize(byte fieldSize) {
        this.fieldSize = fieldSize;
        return this;
    }

    public boolean isSwitchable() {
        return switchable;
    }

    public Settings setSwitchable(boolean switchable) {
        this.switchable = switchable;
        return this;
    }

    public boolean isMusicOn() {
        return musicOn;
    }

    public Settings setMusicOn(boolean musicOn) {
        this.musicOn = musicOn;
        return this;
    }

    public boolean isVibrationOn() {
        return vibrationOn;
    }

    public Settings setVibrationOn(boolean vibrationOn) {
        this.vibrationOn = vibrationOn;
        return this;
    }

    public boolean isAutoSavingOn() {
        return autoSavingOn;
    }

    public Settings setAutoSavingOn(boolean autoSavingOn) {
        this.autoSavingOn = autoSavingOn;
        return this;
    }

    public void save() {
        try {
            FileWriter writer = new FileWriter(settingsFile);
            if (settingsFile.exists() || settingsFile.createNewFile()) {
                writer.write(gson.toJson(this));
                writer.close();
            }
        } catch (IOException e) {
            Log.e("Settings", "Saving", e);
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
